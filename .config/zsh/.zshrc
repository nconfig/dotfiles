# nconfig's zsh init config
 
setopt autocd
autoload -U colors && colors			# setup colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[red]%}@%{$fg[yellow]%}%m%{$fg[red]%}] %{$fg[yellow]%}%~ %{$fg[blue]%}>%{$fg[green]%}>%{$reset_color%}%b "

HISTSIZE=123456789
SAVEHIST=123456789
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/shell_history"
stty stop undef			#		stop from freezing tty (ctrl+s)

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)			# show hidden files in auto-completion

bindkey -v			# vim keybinds
export KEYTIMEOUT=1
bindkey -M menuselect 'h' vi-backward-char			# vim in menuselect
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'j' vi-forward-char

echo -ne '\e[6 q'		# set beam cursor

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh		# nice plugin (must be last)
